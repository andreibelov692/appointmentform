import Vue from "vue";
import Vuex from 'vuex'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        appointments: [{patient: 'Федотов Сергей Петрович', doctor: 'Иванов Иван Сергеевич', startDate: '2023-04-22 20:38', endDate: '2023-04-22 21:38'}],
        doctorFields: [
            'Имя',
            'Фамилия',
            'Отчество',
            'Телефон',
             'Время начала рабочего дня',
             'Время окончания рабочего дня',
             'Дата рождения',
            'Email',
        ],
        patientFields: [
            'Имя',
            'Фамилия',
            'Отчество',
            'СНИЛС',
            'Дата рождения',
            'Место жительства',
        ],
        registrationFields: [
            {field: 'patient', title: 'Пациент'},
            {field: 'doctor', title: 'Врач'},
            {field: 'data', title: 'Дата'}
        ],
        patients: [
            {name: 'Иван', surname: 'Иванов', middleName: 'Сергеевич'},
            {name: 'Петр', surname: 'Петров', middleName: 'Сергеевич'},
        ],
        doctors: [
            {name: 'Иван', surname: 'Иванов', middleName: 'Сергеевич'},
        ]
    },
    getters: {
        patients(state) {
            return state.patients
        },
        doctors(state) {
            return state.doctors
        },
        doctorFields(state) {
            return state.doctorFields
        },
        patientFields(state) {
            return state.patientFields
        },
        appointments(state) {
            return state.appointments
        }
    },
    actions: {

    },
    mutations: {
        addDoctor(state, doctorr) {
            store.state.doctors.push(doctorr)
        },
        addPatient(state, patient) {
            store.state.patients.push(patient)
        },
        addAppointment(state, appointment) {
            store.state.appointments.push(appointment)
        }
    }
});

